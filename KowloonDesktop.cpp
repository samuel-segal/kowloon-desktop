#include <iostream>
#include <list>
#include <fstream>
#include <random>
#include "Structure.h"
#include "StructureAlgorithm.h"
#include "ImageEdit.h"
#include "Render.h"
using namespace std;
using namespace Structure;
using namespace StructureAlgorithm;

int main()
{

	ifstream frontStream, backStream;
	frontStream.open("front.dat");
	backStream.open("back.dat");


	Building front, back;
	if (frontStream.good() && backStream.good()) {
		front = Building::fromStream(frontStream);
		back = Building::fromStream(backStream);
	}
	else {
		front = Building(100, 100);
		back = Building(100, 100);
		front.initTiles();
		back.initTiles();

		tile t;
		t.type = type::ROOM;
		front.setTile(t, 50, 99);
	}


	time_t currentTime;
	currentTime = time(0);
	srand(currentTime);
	TwoLayer a;
	a.setForeground(front);
	a.setBackground(back);
	
	a.step();
	
	drawTwoLayer(a);


	ofstream frontOut,backOut;
	frontOut.open("front.dat");
	backOut.open("back.dat");
	front.toStream(frontOut);
	back.toStream(backOut);

	const wchar_t* path = L"C:/Users/adqui/source/repos/KowloonDesktop/KowloonDesktop/result.png";
	SystemParametersInfoW(SPI_SETDESKWALLPAPER, 0, (void*)path, SPIF_UPDATEINIFILE);

}