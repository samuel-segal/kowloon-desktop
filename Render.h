#pragma once
#include "ImageEdit.h"
#include "StructureAlgorithm.h"
using namespace StructureAlgorithm;



void drawTwoLayer(TwoLayer twoLayer) {
    //TODO Make this code more error tolerant
    const int xRes = 20;
    const int yRes = 16;
    int width = twoLayer.background.width * xRes;
    int height = twoLayer.background.height * yRes;

    GdiplusInit gdiplusinit;
    Bitmap myBitmap(width, height, PixelFormat32bppARGB);
    Graphics g(&myBitmap);
    
    Image* room = Image::FromFile(L"resources/room.png");
    Image* support = Image::FromFile(L"resources/support.png");
    Image* bridge = Image::FromFile(L"resources/bridge.png");
    
    Image* backRoom = Image::FromFile(L"resources/backRoom.png");

    int bindex = 0;
    for (int y = 0; y < twoLayer.background.height; y++) {
        for (int x = 0; x < twoLayer.background.width; x++) {
            switch (twoLayer.background.getTile(bindex).type) {
            case type::ROOM:
                g.DrawImage(backRoom,
                    (REAL)(x * backRoom->GetWidth()),
                    (REAL)(y * backRoom->GetHeight()),
                    (REAL)(backRoom->GetWidth()),
                    (REAL)(backRoom->GetHeight())
                );
            }
            bindex++;
        }
    }


    int index = 0;
    for (int y = 0; y < twoLayer.foreground.height; y++) {
        for (int x = 0; x < twoLayer.foreground.width; x++) {


            Image* out = 0;
            switch (twoLayer.foreground.getTile(index).type) {
            case type::ROOM :
                out = room;
                break;
            case type::SUPPORT:
                out = support;
                break;
            case type::BRIDGE:
                out = bridge;
                break;
            }
            if(out){
                g.DrawImage(out,
                     (REAL)(x* out->GetWidth()),
                    (REAL)(y * out->GetHeight()),
                    (REAL)(out->GetWidth()),
                    (REAL)(out->GetHeight())
                    );
            }

            index++;
        }
    }

    CLSID pngClsid;
    int result = GetEncoderClsid(L"image/png", &pngClsid);
    if (result == -1)
        throw runtime_error("GetEncoderClsid");
    if (Ok != myBitmap.Save(L".//result.png", &pngClsid, NULL))
        throw runtime_error("Bitmap::Save");
    
}