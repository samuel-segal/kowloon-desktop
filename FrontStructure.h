#pragma once

namespace FrontTile {
	enum class TileType {
		AIR,
		ROOM,
		BRIDGE,
		STAIRS,
		DOOR_RIGHT,
		DOOR_LEFT
	};

	struct Tile {
		int x;
		int y;
		TileType type;
	};
}