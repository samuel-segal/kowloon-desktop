#pragma once
#include <fstream>
#include <list>
using namespace std;
namespace Structure {
	enum class type {
		AIR,
		ROOM,
		WALL,
		LADDER,
		STAIRS,
		BRIDGE,
		SUPPORT
	};

	struct tile {
		type type;
	};

	class Building {

	public:
		tile* tiles;
		int width, height;
		Building() {
			this->width = 0;
			this->height = 0;
			this->tiles = {};
		}
		Building(int width, int height) {
			this->width = width;
			this->height = height;
			this->tiles = new tile[width * height];
			
		}
		void initTiles() {
			int size = width * height;
			tiles = new tile[size];
			for (int i = 0; i < size; i++) {
				tile newTile;
				newTile.type = type::AIR;
				tiles[i] = newTile;
			}
		}

		static Building fromStream(istream &stream) {
			int w, h;
			stream.read((char*)&w,sizeof(w));
			stream.read((char*)&h, sizeof(h));

			Building newB(w, h);
			int indexes = w *h;
			newB.tiles = new tile[indexes];
			for (int i = 0; i < indexes; i++) {
				tile newTile;
				type tiletype;
				stream.read((char*)&tiletype, sizeof(tiletype));

				newTile.type = tiletype;
				
				newB.tiles[i] = newTile;
			}
			

			return newB;
		}

		void toStream(ostream &stream) {
			stream.write((char*)&width, sizeof(width));
			stream.write((char*)&height, sizeof(height));
			int indexes = width * height;
			for (int i = 0; i < indexes; i++) {
				type tiletype = tiles[i].type;
				stream.write((char*)&tiletype, sizeof(type));
			}


		}

		tile getTile(int index) {
			return tiles[index];
		}

		tile getTile(int x, int y) {
			return tiles[x + y * width];
		}

		void setTile(tile value, int x, int y) {
			tiles[x + y * width] = value;
		}
	};
	
}