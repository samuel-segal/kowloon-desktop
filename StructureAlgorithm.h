#pragma once
#include <list>
#include <random>
#include <math.h>
#include "Structure.h"
using namespace std;
using namespace Structure;

namespace StructureAlgorithm {
	
	class Algorithm {
		virtual void step() = 0;
	};
	
	class TwoLayer :Algorithm {
	public:
		Building background, foreground;
		TwoLayer() {
			xDistrib = normal_distribution<float>(0, 3);
			yDistrib = normal_distribution<float>(0, 2);
		}
		void setBackground(Building b1) {
			this->background = b1;
		}

		void setForeground(Building b2) {
			this->foreground = b2;
		}

		void step() {
			list<int> tileList = getTiles(foreground);
			float chance = tileList.size() < 30 ? 1 : 30.0f / tileList.size();
			
			for (int i = 0; i < tileList.size(); i++) {
				int currentRandom = tileList.front();
				tileList.pop_front();
				float randomN = ((float)rand() / RAND_MAX);
				if (randomN <= chance) {
					int x = currentRandom % foreground.width;
					int y = currentRandom / foreground.width;

					int dx = round(xDistrib(engine));
					int dy = round(yDistrib(engine));

					int nx = x + dx;
					int ny = y + dy;

					//Places the tile
					if (
						nx >= 0
						&& nx < foreground.width
						&& ny >= 0
						&& ny < foreground.height
						) {
						if (foreground.getTile(nx, ny).type == type::AIR) {
							bool hasAbove = false;
							int ay = ny;
							while (ay < foreground.height - 1) {
								if (foreground.getTile(ay, ny).type != type::AIR) {
									hasAbove = true;
									break;
								}
								ay--;
							}

							if (hasAbove) {
								createForegroundRoom(x, y, dx, dy);
							}
						}
						else {
							tile t;
							t.type = type::ROOM;
							background.setTile(t, x, y);
						}
					}
				}
			}
		
		}

	private:
		default_random_engine engine;
		normal_distribution<float> xDistrib;
		normal_distribution<float> yDistrib;
		void createForegroundRoom(int x, int y, int dx, int dy) {
			
			int newX = x + dx;
			int newY = y + dy;
			tile t;
			t.type = type::ROOM;
			
			foreground.setTile(t, newX, newY);
			if (dy == 0) {
				createBridge(newX,newY);
				createScaffolding(newX, newY);
			}
			
		}

		//TODO Create scaffolding
		void createScaffolding(int x, int y) {
			y++;
			while (y < foreground.height - 1 && foreground.getTile(x, y).type == type::AIR) {
				tile t;
				t.type = type::SUPPORT;
				foreground.setTile(t,x,y);

				y++;
			}
		}

		void createBridge(int newX, int newY) {

			//TODO Optimize this so it fits in only one while loop
			int leftRoom = 0, rightRoom = 0;
			int lx = newX, rx = newX;
			bool hasLeft = false, hasRight = false;
			while (lx > 0) {
				lx--;
				leftRoom++;
				if (foreground.getTile(lx, newY).type == type::ROOM) {
					hasLeft = true;
					break;
				}

			}
			while (rx < foreground.width) {
				rx++;
				rightRoom++;
				if (foreground.getTile(rx, newY).type == type::ROOM) {
					hasRight = true;
					break;
				}

			}

			if (hasLeft||hasRight) {
				bool whichBigger = leftRoom <= rightRoom;
				
				tile t;
				t.type = type::BRIDGE;
				if (whichBigger) {
					for (int i = 1; i < leftRoom; i++) {
						foreground.setTile(t, newX - i, newY);
					}
				}
				else {
					for (int i = 1; i < rightRoom; i++) {
						foreground.setTile(t, newX + i, newY);
					}
				}

			}
			
		}
		
		bool hasSurrounding(Building b, int x, int y, type tile) {
			return
				(x > 0 && b.getTile(x - 1, y).type == tile)
				|| (y > 0 && b.getTile(x, y - 1).type == tile)
				|| (x < b.width - 1 && b.getTile(x + 1, y).type == tile)
				|| (y < b.height - 1 && b.getTile(x, y + 1).type == tile);

		}
		list<int> getTiles(Building b) {
			list<int> output;
			int index = 0;
			for (int y = 0; y < b.height; y++) {
				for (int x = 0; x < b.width; x++) {
					if (b.tiles[index].type == type::ROOM) {
						output.push_front(index);
					}
					index++;
				}
			}
			return output;
		}

	};
}